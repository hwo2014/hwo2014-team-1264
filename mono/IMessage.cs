﻿using System;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace logv
{
    public class Message
    {
        [JsonProperty("msgType")]
        public string MessageType { get; set;}

        [JsonProperty("data")]
        public Object Data{ get; set; }

        [JsonProperty("gameTick")]
        public int GameTicks { get; set; }
    }
}

