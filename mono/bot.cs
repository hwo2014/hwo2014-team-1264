namespace logv {
    using System;
    using System.IO;
    using System.Net.Sockets;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using logv.dataflow;
    using System.Reactive.Concurrency;
    using System.Reactive.Disposables;
    using System.Reactive.Joins;
    using System.Reactive;
    using System.Reactive.Subjects;
    using System.Reactive.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Threading;
    
    public class Bot {

        private static Action<string> Write;
        private static IEnumerable<string> ReadLines(TextReader reader)
        {
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                yield return line;
            }
                
        }


      
        public static void Main(string[] args) {
            string host = args[0];
            int port = int.Parse(args[1]);
            string botName = args[2];
            string botKey = args[3];
    
            Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
    

    
            using (TcpClient client = new TcpClient(host, port))
            {
                NetworkStream stream = client.GetStream();
                StreamReader reader = new StreamReader(stream);
                var streamWriter = new StreamWriter(stream);
                TextWriter writer = TextWriter.Synchronized(streamWriter);
                streamWriter.AutoFlush = true;
  
                Write = writer.WriteLine;
                var throttle = new BehaviorSubject<double>(0.0);
                var myPosition = new BehaviorSubject<Car>(null);
                var mySpeed = new BehaviorSubject<double>(0);
               

                var lines = ReadLines(TextReader.Synchronized(reader));
                var messages = lines.Select(ParseMessage).ToObservable(TaskPoolScheduler.Default).Publish();
                messages.Connect();
                messages.Subscribe(Console.WriteLine);
                messages.OfType<GameInitMessage>().Subscribe(m =>
                {
                    foreach (var item in m.Track.Pieces)
                    {
                        Console.WriteLine(item);
                    }
                });

                myPosition.Buffer(2, 1).Subscribe(s => {
                    var current = s.First();
                    if(current == null)
                        return;
                    var last = s.Last();
                    var speed = last.PiecePosition.InPieceDistance - current.PiecePosition.InPieceDistance ;
                    mySpeed.OnNext(speed);
                });

                throttle.Subscribe(t => Write(JsonConvert.SerializeObject(new {msgType = "throttle", data = t})));
                mySpeed.Subscribe(s => Console.WriteLine(string.Format("NEW SPEED {0}", s)));

                messages.OfType<CarPositionsMessage>().Subscribe((m) =>
                {
                    var we = m.Cars.First(it => it.Id.Name.Equals(botName));

                    myPosition.OnNext(we);

                    foreach (var car in m.Cars) {
                        Console.WriteLine(car.Id.Name);
                    }
                    throttle.OnNext(0.61);

                });
                new Bot(reader, streamWriter, new Join(botName, botKey));
                messages.Wait();
                Console.WriteLine("END");
            }
        }
       
            
        private static ServerMessage ParseMessage(string s)
        {
            //Console.WriteLine("#########");
            //Console.WriteLine(s);
            //Console.WriteLine("#########");
            try
            {
                var msg = JsonConvert.DeserializeObject<Message>(s);
                switch (msg.MessageType)
                {
                    case "yourCar":
                        return new YourCarMessage((JToken)msg.Data);
                        break;
                    case "gameInit":
                        return new GameInitMessage((JToken)msg.Data);
                        break;
                    case "gameStart":
                        return new GameStartMessage();
                        break;
                    case "carPositions":
                        return new CarPositionsMessage((JToken)msg.Data, msg.GameTicks);
                        break;
                    default:
                        Console.WriteLine(msg.MessageType);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.WriteLine("msg: " +s);
                Console.WriteLine("#############");
            }
           
            return null;

        }


    
        private StreamWriter writer;
    
        Bot(StreamReader reader, StreamWriter writer, Join join) {
            this.writer = writer;
            string line;
    
            send(join);
    
            /*while((line = reader.ReadLine()) != null) {
                MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                switch(msg.msgType) {
                    case "carPositions":
                        send(new Throttle(0.6));
                        break;
                    case "join":
                        Console.WriteLine("Joined");
                        send(new Ping());
                        break;
                    case "gameInit":
                        Console.WriteLine("Race init");
                        send(new Ping());
                        break;
                    case "gameEnd":
                        Console.WriteLine("Race ended");
                        send(new Ping());
                        break;
                    case "gameStart":
                        Console.WriteLine("Race starts");
                        send(new Ping());
                        break;
                    default:
                        send(new Ping());
                        break;
                }
            }*/
        }
    
        private void send(SendMsg msg) {
            writer.WriteLine(msg.ToJson());
        }
    }
    
    class MsgWrapper {
        public string msgType;
        public Object data;
    
        public MsgWrapper(string msgType, Object data) {
            this.msgType = msgType;
            this.data = data;
        }
    }
    
    abstract class SendMsg {
        public string ToJson() {
            return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
        }
        protected virtual Object MsgData() {
            return this;
        }
    
        protected abstract string MsgType();
    }
    
    class Join: SendMsg {
        public string name;
        public string key;
        public string color;
    
        public Join(string name, string key) {
            this.name = name;
            this.key = key;
            this.color = "red";
        }
    
        protected override string MsgType() { 
            return "join";
        }
    }
    
    class Ping: SendMsg {
        protected override string MsgType() {
            return "ping";
        }
    }
    
    class Throttle: SendMsg {
        public double value;
    
        public Throttle(double value) {
            this.value = value;
        }
    
        protected override Object MsgData() {
            return this.value;
        }
    
        protected override string MsgType() {
            return "throttle";
        }
    }
}