﻿public class PIDController
{
    public enum PIDMode
    {
        Manual,
        Auto,
    }

    public enum PIDAction
    {
        Indirect,
        Direct,
    }

    public PIDMode Mode { get; set; }
    public PIDAction Action { get; set; }

    public double Proportional { get; private set; }
    public double Integral { get; private set; }
    public double Derivative { get; private set; }

    public double Minimum { get; private set; }
    public double Maximum { get; private set; }

    public double DeltaMinimum { get; private set; }
    public double DeltaMaximum { get; private set; }

    private double _ProportionalTerm;
    private double _Integrator;
    private double _Derivator;

    public double Setpoint { get; set; }

    private double _Feedback;
    public double Feedback
    {
        get
        {
            return _Feedback;
        }
    }

    public PIDController(double proportional, double integral, double derivative, double min, double max, double dmax, double dmin)
    {
        this.Proportional = proportional;
        this.Integral = integral;
        this.Derivative = derivative;
        this.Minimum = min;
        this.Maximum = max;
        this.DeltaMinimum = dmin;
        this.DeltaMaximum = dmax;
    }

    public void Calculate(double Input, long Time)
    {
        double output;

        // Compute the error value
        double Error = Setpoint - Input;

        if (Mode == PIDMode.Auto)
        {
            if (Action == PIDAction.Direct)
                Error = 0 - Error;

            // Compute the proportional component
            _ProportionalTerm = 1000.0f * (Error - _Derivator) / (double)Time;

            // Compute the integrator component, clamped to min/max delta movement
            _Integrator += (float)Error * (float)Time / 1000.0f;
            if (_Integrator < DeltaMinimum)
                _Integrator = DeltaMinimum;
            if (_Integrator > DeltaMaximum)
                _Integrator = DeltaMaximum;

            // Add the proportional component
            output = (Proportional * Error);

            // Add the integral component
            output += Integral * _Integrator;

            // Add the derivative component
            output += Derivative * _ProportionalTerm;

            // Clamp output to min/max
            if (output < Minimum)
                output = Minimum;
            if (output > Maximum)
                output = Maximum;
        }
        else
        {
            output = Input;
        }

        // Store values
        _Derivator = Error;

        // Returns the result
        _Feedback = output;
    }
}