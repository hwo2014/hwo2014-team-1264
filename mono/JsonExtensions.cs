﻿using System;
using Newtonsoft.Json.Linq;

namespace logv
{
    public static class JsonExtensions
    {
        public static T SafeValue<T>(this JToken t)
        {
            if(t != null)
                return t.Value<T>();

            return default(T);
        }
    }
}

