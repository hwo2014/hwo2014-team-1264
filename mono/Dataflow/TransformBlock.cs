﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace logv.dataflow
{
    public class TransformBlock<TSource, TTarget> : IValueSource<TSource>, IValueTarget<TTarget>
    {
        private readonly Func<TTarget, TSource> _producer;
        private readonly ConcurrentQueue<TTarget> _queue = new ConcurrentQueue<TTarget>();
        private IValueTarget<TSource> _next;

        public TransformBlock(Func<TTarget, TSource> producer)
        {
            _producer = producer;
        }

        #region IValueTarget implementation

        public void Post(TTarget val)
        {
            _queue.Enqueue(val);
            Run();
        }

        #endregion

        #region IValueSource implementation

        public TSource CreateValue()
        {
            TTarget msg = default(TTarget);
            if (_queue.TryDequeue(out msg))
                return _producer(msg);

            return default(TSource);
        }

        public void LinkTo(IValueTarget<TSource> target)
        {
            _next = target;
            Run();
        }

        #endregion

        private void Run()
        {
            if (_next == null)
                return;
                
            Task.Factory.StartNew(() =>
            {
                TSource val = CreateValue();
                while (val != null)
                {
                    _next.Post(val);
                    val = CreateValue();
                }
            });
        }
    }
}

