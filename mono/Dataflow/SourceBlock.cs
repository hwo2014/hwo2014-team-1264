﻿using System;

namespace logv.dataflow
{
    public class SourceBlock<T> : IValueSource<T>
    {
        private readonly Func<T> _source;
        public SourceBlock(Func<T> source)
        {
            _source = source;
        }

        #region IValueSource implementation

        public T CreateValue()
        {
            throw new NotImplementedException();
        }

        public void LinkTo(IValueTarget<T> target)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}

