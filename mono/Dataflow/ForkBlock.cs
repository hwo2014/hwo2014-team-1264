﻿using System;
using System.Collections.Generic;

namespace logv.dataflow
{
    public class ForkBlock<T> : IValueSource<T>, IValueTarget<T>
    {
        private readonly IList<IValueTarget<T>> targets = new List<IValueTarget<T>>();
        public ForkBlock()
        {
        }

        #region IValueTarget implementation

        public void Post(T val)
        {
            foreach (var target in targets)
            {
                target.Post(val);
            }
        }

        #endregion

        #region IValueSource implementation

        public T CreateValue()
        {
            throw new NotImplementedException();
        }

        public void LinkTo(IValueTarget<T> target)
        {
            targets.Add(target);
        }

        #endregion
    }
}

