﻿using System;
using System.Threading.Tasks;

namespace logv.dataflow
{
    public class ActionBlock<TTarget> : IValueTarget<TTarget>
    {
        private readonly Action<TTarget> _action;
        public ActionBlock(Action<TTarget> action)
        {
            this._action = action;
        }

       
        #region IValueTarget implementation
        public void Post(TTarget val)
        {
            Task.Factory.StartNew(() => _action(val));
        }
        #endregion
    }
}

