﻿using System;

namespace logv.dataflow
{
    public interface IValueTarget<in T>
    {
        void Post(T val);
    }
}

