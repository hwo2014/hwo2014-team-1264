﻿using System;

namespace logv.dataflow
{
    public interface IValueSource<out T>
    {
        T CreateValue();
        void LinkTo(IValueTarget<T> target);
    }
}

