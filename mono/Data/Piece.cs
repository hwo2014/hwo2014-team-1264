﻿using System;
using Newtonsoft.Json.Linq;

namespace logv
{
    public class Piece
    {
        public double Length{ get; set; }
        public bool HasSwith{ get; set; }
        public double Angel { get;set;}
        public int Radius { get; set; }
        public Piece()
        {
        }

        public static Piece Create(JToken t)
        {
            return new Piece()
            {
                Length = t["length"].SafeValue<double>(),
                HasSwith = t["switch"] != null,
                Angel = t["angle"].SafeValue<double>(),
                Radius = t["radius"].SafeValue<int>()
            };
        }
    }
}

