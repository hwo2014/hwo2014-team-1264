﻿using System;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace logv
{
    public class YourCarMessage : ServerMessage
    {
        private readonly JToken o;
        public YourCarMessage(JToken o)
        {
            this.o = o;
        }

        public string Color { get {return (string)o["color"]; } }
        public string Name { get { return (string)o["name"]; } }
    }
}

