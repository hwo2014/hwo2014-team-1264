using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace logv
{
	public class Id
	{
        private readonly JToken t;
        public string Name{get{return (string)t["name"]; }}
        public string Color {get {return (string)t["color"]; }}
        public Id(JToken t)
        {
            this.t = t;   
        }
	}
}

