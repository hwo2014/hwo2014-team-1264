using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace logv
{
    public class Car
    {
        private readonly JToken t;
        public Id Id{ get; set;}
        public int Lap { get {return (int)t["lap"]; } }
        public double Angel {get {return (double)t["angel"]; }}
        public PiecePosition PiecePosition{ get { return new PiecePosition(t["piecePosition"]); } }

        public Car(JToken o)
        {
            this.t = o;
            this.Id = new Id(t["id"]);

        }


    }
}

