﻿using System;
using Newtonsoft.Json.Linq;

namespace logv
{
    public class GameInitMessage : ServerMessage
    {
        private readonly JToken o;
        public Track Track{ get; set; }
        public GameInitMessage(JToken o)
        {
            this.o = o;
            this.Track = new Track(o);
        }


    }
}

