﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace logv
{
    public class CarPositionsMessage : ServerMessage
    {
        private readonly JToken o;
        public CarPositionsMessage(JToken o, int ticks)
        {
            this.o = o;
            this.Ticks = ticks;
        }
        public IEnumerable<Car> Cars{ get{ return o.Select(t => new Car(t));} }
        public int Ticks{ get; private set;}
    }
}
