﻿using System;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace logv
{
    public class Track
    {
        private readonly JToken o;
        public Track(JToken o)
        {
            this.o = o["race"]["track"];
        }

        public string Id { get {return (string)o["id"]; } }
        public string Name { get { return (string)o["name"]; } }
        public IEnumerable<Piece> Pieces { get { return o["pieces"].Select(Piece.Create);}}

    }
}

