using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace logv
{
	public class PiecePosition
	{
        private readonly JToken t;
        public int PieceIndex {get {return t["pieceIndex"].SafeValue<int>(); }}
        public double InPieceDistance{get {return t["inPieceDistance"].SafeValue<double>(); }}
        public PiecePosition(JToken t)
        {
            this.t = t;
        }
	}
}

